#ifndef DRAW_UTILS_INCLUDED
#define DRAW_UTILS_INCLUDED

#include<glew.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

// Load a file into an OpenGL texture, and return that texture.
GLuint glTexImageTGAFile(const char* filename, int* outWidth, int* outHeight);

// Draw the sprite
void glDrawSprite(GLuint tex, int x, int y, int w, int h);

#endif