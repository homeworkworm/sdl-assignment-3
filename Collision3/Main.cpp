// #define _CRT_SECURE_NO_WARNINGS
#define SDL_MAIN_HANDLED

#include <stdlib.h>
#include<stdio.h>
#include <iostream>

#include <string.h>
#include <vector>
#include <time.h>
#include <random>
#include <algorithm>
#include <iterator>

#include <SDL.h>
#include <glew.h>

#include "DrawUtils.h"
#include "Animation.h"

using namespace std;

int const Screen_Width = 1024;
int const Screen_Height = 768;
int maxX = Screen_Width * 3;
int maxY = Screen_Height * 3;
int camXMax = 1792;
int camYMax = 1280;

// Update the animation, and stop when end
void animTick(AnimData* data, float d)
{
	if (!data->play)
	{
		return;
	}
	int numFrames = data->def->frames.size();
	data->timeToNextFrame -= d;

	if (data->timeToNextFrame < 0)
	{
		++data->curFrame;
		if (data->curFrame >= numFrames && data->loop == true)
		{
			data->curFrame = 0;
		}

		if (data->curFrame >= numFrames && data->loop == false)
		{
			data->curFrame = numFrames - 1;
			data->timeToNextFrame = 0;
			data->play = false;
		}
		else
		{
			AnimFrameDef* curFrame = &data->def->frames[data->curFrame];
			data->timeToNextFrame += curFrame->frameTime;
		}
	}
}

void setData(AnimData* data, AnimDef* def, int x, int y, int width, int height, bool loop, bool move, bool pos)
{
	data->x = x;
	data->y = y;
	data->loop = loop;
	data->w = width;
	data->h = height;
	data->def = def;
	data->move = move;
	data->pos = pos;
	data->curFrame = 0;
	data->timeToNextFrame = 0;
	data->play = false;
}

//Setup the definition and textures
AnimDef setAnimDef(char* name, int frameN, int totalFrameN, float frameTime)
{
	AnimDef animDef;
	animDef.name = name;
	for (int i = 0; i < totalFrameN; i++)
	{
		AnimFrameDef frame;
		frame.frameNum = i;
		frame.frameTime = frameTime;
		animDef.frames.push_back(frame);
		char f[20];
		//loop
		if (i % totalFrameN <= frameN)
		{
			sprintf_s(f, "%d", i % totalFrameN);
		}
		else
		{
			sprintf_s(f, "%d", totalFrameN - i % totalFrameN);
		}
		char fileName[256];
		strncpy_s(fileName, animDef.name, sizeof(fileName));
		strncat_s(fileName, f, sizeof(fileName));
		strncat_s(fileName, ".tga", sizeof(fileName));
		animDef.tex.push_back(glTexImageTGAFile(fileName, 0, 0));
	}
	return animDef;
}

// Move vertically
void move(AnimData* data, bool pos)
{
	if (pos)
	{
		data->y--;
		if (data->y >= maxY)
		{
			data->loop = false;
		}
	}
	else
	{
		data->y++;
		if (data->y <= 0 - data->h)
		{
			data->loop = false;
		}
	}
}

int main(void)
{

	Sprite sprite;
	cout << sprite.x << endl;
	char *input = NULL;
	scanf_s(input);

	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		return EXIT_FAILURE;
	}

	// Create the window, OpenGL context
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_Window *window = SDL_CreateWindow(
	                       "Game with SDL",
	                       SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	                       Screen_Width, Screen_Height,
	                       SDL_WINDOW_OPENGL | SDL_WINDOW_MAXIMIZED);
	if (!window)
	{
		SDL_Quit();
		return EXIT_FAILURE;
	}

	SDL_GL_CreateContext(window);
	// Make sure we have a recent version of OpenGL
	GLenum glewError = glewInit();

	if (glewError != GLEW_OK)
	{
		SDL_Quit();
		return EXIT_FAILURE;
	}
	if (!GLEW_VERSION_1_5)
	{
		SDL_Quit();
		return EXIT_FAILURE;
	}

	glViewport(0, 0, Screen_Width, Screen_Height);
	glMatrixMode(GL_PROJECTION);
	glOrtho(0, Screen_Width, Screen_Height, 0, 0, 1);
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.04);

	//world setup
	int quit = 0;
	int camX = 0;
	int camY = 0;
	int step = 8;

	//background setup
	int tileLength = 64;
	int outWidth = 0;
	int outHeight = 0;
	const int rows = 30;
	const int columns = 42;
	int **back = new int*[rows];

	GLuint tex[rows * columns];
	int i = 0;
	for (int r = 0; r < rows; ++r)
	{
		back[r] = new int[columns];
		for (int c = 0; c < columns; ++c)
		{
			char t[20];
			back[r][c] = 1;
			sprintf_s(t, "%d", back[r][c]);
			char fileName[256];
			strncpy_s(fileName, "picture/", sizeof(fileName));
			strncat_s(fileName, t, sizeof(fileName));
			strncat_s(fileName, ".tga", sizeof(fileName));
			tex[i] = glTexImageTGAFile(fileName, &outWidth, &outHeight);
			i++;
		}
	}

	//sprite setup
	AnimData pocketmon;
	AnimationDef def0 = setAnimDef("dbz/pocket/", 3, 3, 160);
	setData(&pocketmon, &def0, 0, 0, 50, 70, true, true, false);

	vector<AnimData> picture;
	int pictureSize = 10000;
	int temp = 0;
	for (int i = 0; i < pictureSize; i++)
	{
		picture.push_back(pocketmon);
		picture[i].x = i % (maxX - picture[i].h);
	}

	random_shuffle(picture.begin(), picture.end());

	float timeCurrentFrame = (float)SDL_GetTicks();
	float timeSinceLastFrame = 0;

	while (!quit)
	{
		// Handle OS message pump
		const unsigned char* keys = SDL_GetKeyboardState(NULL);

		timeSinceLastFrame = SDL_GetTicks() - timeCurrentFrame;
		timeCurrentFrame = timeCurrentFrame + timeSinceLastFrame;

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = 1;
				goto SUCESSFULLY_QUIT;
			}
		}

		// Game logic goes here

		//scrolling
		if (keys[SDL_SCANCODE_UP] && camY > 0)
		{
			camY -= step;
		}
		if (keys[SDL_SCANCODE_DOWN] && camY < camYMax)
		{
			camY += step;
		}
		if (keys[SDL_SCANCODE_LEFT] && camX > 0)
		{
			camX -= step;
		}
		if (keys[SDL_SCANCODE_RIGHT] && camX < camXMax)
		{
			camX += step;
		}

		//draw background
		int x = 0;
		int y = 0;
		i = 0;
		for (int r = 0; r < rows; ++r)
		{
			x = 0;
			for (int c = 0; c < columns; ++c)
			{
				glDrawSprite(tex[i], x - camX, y - camY, tileLength, tileLength);
				i++;
				x += tileLength;
			}
			y += tileLength;
		}

		//add sprites
		if (keys[SDL_SCANCODE_A])
		{
			if (temp < picture.size())
			{
				picture[temp].play = true;
				if (temp != picture.size() - 1)
				{
					temp++;
				}
			}

		}
		//remove sprites
		if (keys[SDL_SCANCODE_R])
		{
			if (temp >= 0)
			{
				picture[temp].play = false;
				if (temp != 0)
				{
					temp--;
				}
			}
		}

		//animate sprites
		for (int i = 0; i < picture.size(); i++)
		{
			if (picture[i].play == true)
			{
				glDrawSprite(picture[i].def->tex[picture[i].curFrame], picture[i].x - camX, picture[i].y - camY, picture[i].w, picture[i].h);
				animTick(&picture[i], timeSinceLastFrame);

				if (picture[i].move)
				{
					move(&picture[i], picture[i].pos);
				}
			}
		}

		SDL_GL_SwapWindow(window);
	}

SUCESSFULLY_QUIT:
	SDL_Quit();
	return EXIT_SUCCESS;
}