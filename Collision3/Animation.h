#ifndef ANIMATION_INCLUDED
#define ANIMATION_INCLUDED

typedef struct AnimData AnimData;
typedef struct AnimDef AnimationDef;
typedef struct AnimFrameDef AnimFrameDef;

// Animation states
struct AnimData
{
	AnimationDef* def;
	int curFrame;
	float timeToNextFrame;
	int x;
	int y;
	int w;
	int h;
	bool play;
	bool loop;
	bool move;
	bool pos;
};

// Defines charactar
struct AnimDef
{
	const char* name;
	std::vector<AnimFrameDef> frames;
	std::vector<GLuint> tex;
};

// Defines a frame
struct AnimFrameDef
{
	int frameNum;
	float frameTime;
};

class Sprite
{
public:
	int x, y;

	Sprite()
	{
		x = 2;
		y = 3;
	}
};

#endif